// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeProject, "SnakeProject" );
